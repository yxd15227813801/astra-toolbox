#include <cpp/creators.hpp>
#include <astra/CudaSirtAlgorithm.h>

#include <iostream>
#include <fstream>

using namespace std;
using namespace astra;

void rotate_z(double angle, double x, double y, double *x_o, double *y_o)
{
    *x_o = cos(angle) * x - sin(angle) * y;
    *y_o = sin(angle) * x + cos(angle) * y;
}

int main (int argc, char *argv[])
{
    int i;
    float32 *angles;
    uint16_t *slice;
    float32 *slice_f;
    CVolumeGeometry2D *vol_geom;
    CFanFlatVecProjectionGeometry2D *proj_geom;
    CCudaProjector2D *proj;
    CFloat32ProjectionData2D *sino;
    CFloat32VolumeData2D *rec;
    CCudaSirtAlgorithm *algo_sirt;
    double *vectors;
    ifstream file;
    double gamma;
    double src[2],det[2],pix_u[2];
    double e_w[2],e_u[2];

    const double pix = 0.1; // in mm
    const double R = 295.195; // in mm 
    const double D = 446.986; // in mm
    const double u_0 = -3.63942; // in detector pixels
    const double slant = -0.393957; // in radians?

    const int nproj = 600;
    const int det_perp = 1120;

    // load sinogram data
    slice = new uint16_t[det_perp*nproj];
    file.open("slice.dat",ios::in|ios::binary);
    file.read((char*)slice,det_perp*nproj*sizeof(uint16_t));
    file.close();
    slice_f = new float32[det_perp*nproj];
    for(i=0;i<det_perp*nproj;i++)
        slice_f[i] = (float32)slice[i];
    delete[] slice;

    // volume geometry
    vol_geom = create_vol_geom_2d(560,560,-25,25,-35,15);

    // projection geometry
    angles = create_angles(0,2*M_PI,nproj);
    vectors = new double[6*nproj];

    e_u[0] = -sin(slant);
    e_u[1] =  cos(slant);

    e_w[0] = cos(slant);
    e_w[1] = 0;

    // geometry at projection angle 0
    // cone vertex
    src[0] = R;
    src[1] = 0;
    // vector from det pixel (0,0) to (0,1)
    pix_u[0] = 0   * e_u[0];
    pix_u[1] = pix * e_u[1];
    // center of detector
    det[0] = R - D * e_w[0] + u_0 * pix_u[0];
    det[1] = 0 - 0 * e_w[1] + u_0 * pix_u[1];

    // rotate x -> -y
    gamma = -M_PI_2;
    rotate_z(gamma,src[0],src[1],&src[0],&src[1]);
    rotate_z(gamma,det[0],det[1],&det[0],&det[1]);
    rotate_z(gamma,pix_u[0],pix_u[1],&pix_u[0],&pix_u[1]);

    // rotate by projection angle around Z
    for(i=0;i<nproj;i++) {
        gamma = angles[i];
        rotate_z(gamma,src[0],src[1],&vectors[6*i+0],&vectors[6*i+1]);
        rotate_z(gamma,det[0],det[1],&vectors[6*i+2],&vectors[6*i+3]);
        rotate_z(gamma,pix_u[0],pix_u[1],&vectors[6*i+4],&vectors[6*i+5]);
    }
    proj_geom = create_proj_geom_2d_fanflat_vec(det_perp,nproj,vectors);
    delete[] vectors;

    // projector
    proj = create_projector_2d_cuda(proj_geom,vol_geom);

    // sinogram data
    sino = create_data_2d_sino(proj_geom,slice_f);

    // reconstruction data
    rec = create_data_2d_vol(vol_geom,0.0);

    // algorithm
    algo_sirt = new CCudaSirtAlgorithm();
    algo_sirt->initialize(proj,sino,rec);
    algo_sirt->setConstraints(true,0.0,false,0.0);
    algo_sirt->run(150);
    delete algo_sirt;

    // write reconstruction data
    save_data_2d("reconstruction_slice.dat",rec);

    delete[] slice_f;
    delete vol_geom;
    delete[] angles;
    delete proj_geom;
    delete proj;
    delete sino;
    delete rec;
    
    return 0;
}
