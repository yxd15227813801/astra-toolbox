#include "cpp/creators.hpp"
#include "astra/CompositeGeometryManager.h"
#include "astra/Logging.h"

#include <iostream>
#include <fstream>
#include <omp.h>
#include <sys/time.h>

using namespace std;
using namespace astra;

void rotate_z(double angle, double x, double y, double z, double *x_o, double *y_o, double *z_o)
{
    *x_o = cos(angle) * x - sin(angle) * y;
    *y_o = sin(angle) * x + cos(angle) * y;
    *z_o = z;
}

int main (int argc, char *argv[])
{
    int i;
    long ix;
    float32 *angles;
    float32 *sino_full_f;
    CVolumeGeometry3D *vol_geom;
    CConeVecProjectionGeometry3D *proj_geom;
    CCudaProjector3D *proy;
    CFloat32ProjectionData3DMemory *sinoData,*projData,*lineWeight;
    CFloat32VolumeData3DMemory *volumeData,*tmpData,*pixelWeight;
    double *vectors;
    ifstream fs;
    double gamma;
    double src[3],det[3],pix_u[3],pix_v[3];
    double e_w[3],e_u[3],e_v[3],alpha[3],beta[3];
    float32 *ptmp;
    const float32 *ctmp,*ctmp2;
    struct timeval tp;
    double t0,t1,t2,dt;

    //CLogger::enableScreen();
    //CLogger::setOutputScreen(2,LOG_DEBUG);

    CCompositeGeometryManager *cgeomgr = new CCompositeGeometryManager();

    // cone beam parameters
    const double pix = 0.1; // in mm
    const double R = 295.195; // in mm
    const double D = 446.986; // in mm
    const double u_0 = -3.63942; // in mm
    const double v_0 = -1.35506; // in mm
    const double twist = -0.129224 * M_PI / 180.0; // in degrees
    const double slant = -0.393957 * M_PI / 180.0; // in degrees
    const double tilt  = -0.167292 * M_PI / 180.0; // in degrees
    const double eta = twist;
    const double theta = slant;
    const double phi = tilt;

    // sinogram dimensions
    const int nproj = 600; // angular resolution
    const int det_perp = 1120; // u resolution
    const int det_axial = 1120; // v resolution

    // reconstruction geometry
    const double voxel_size = 0.035; // in mm
    const int xres = 286*2;
    const int yres = 286*2;
    const int zres = 286*2;
    // FOV center
    const double x0 =  0.0; // in mm
    const double y0 =-12.0; // in mm
    const double z0 =  0.0; // in mm
    // volume limits
    const double xmin = x0 - (voxel_size * xres) * 0.5;
    const double xmax = x0 + (voxel_size * xres) * 0.5;
    const double ymin = y0 - (voxel_size * yres) * 0.5;
    const double ymax = y0 + (voxel_size * yres) * 0.5;
    const double zmin = z0 - (voxel_size * zres) * 0.5;
    const double zmax = z0 + (voxel_size * zres) * 0.5;

    char *file;
    char fname[1024];

    if(argc < 2) {
        fprintf(stderr,"[ERROR] Please supply a sinogram file\n");
        exit(EXIT_FAILURE);
    }

    file = realpath(argv[1],NULL);
    if(file == NULL) {
        fprintf(stderr,"[ERROR] Cannot resolve path: %s\n",argv[1]);
        exit(EXIT_FAILURE);
    }

    // load sinogram data
    sino_full_f = new float32[det_perp*nproj*det_axial];
    fs.open(file,ios::in|ios::binary);
    if(!fs) { fprintf(stderr,"[ERROR] Sinogram file not found: %s\n",file); exit(EXIT_FAILURE); }
    fs.read((char*)sino_full_f,det_perp*nproj*det_axial*sizeof(float32));
    if(!fs) { fprintf(stderr,"[ERROR] Failed sino read\n"); exit(EXIT_FAILURE); }
    fs.close();

    // create volume geometry
    vol_geom = create_vol_geom_3d(xres,yres,zres,xmin,xmax,ymin,ymax,zmin,zmax);

    /* ==================
       CONE BEAM GEOMETRY
       ================== */

    // normal to detector plane
    e_w[0] = cos(theta) * cos(phi);
    e_w[1] = cos(theta) * sin(phi);
    e_w[2] = sin(theta);

    // x orthogonal direction from e_w
    alpha[0] = -sin(phi);
    alpha[1] =  cos(phi);
    alpha[2] =  0;

    // y orthogonal direction from e_w
    beta[0] = -sin(theta) * cos(phi);
    beta[1] = -sin(theta) * sin(phi);
    beta[2] =  cos(theta);

    // x direction along detector plane (including in-plane rotation)
    e_u[0] = cos(eta) * alpha[0] + sin(eta) * beta[0];
    e_u[1] = cos(eta) * alpha[1] + sin(eta) * beta[1];
    e_u[2] = cos(eta) * alpha[2] + sin(eta) * beta[2];

    // y direction along detector plane (including in-plane rotation)
    e_v[0] = cos(eta) * beta[0] - sin(eta) * alpha[0];
    e_v[1] = cos(eta) * beta[1] - sin(eta) * alpha[1];
    e_v[2] = cos(eta) * beta[2] - sin(eta) * alpha[2];

    // geometry at projection angle 0
    // cone vertex (source)
    src[0] = R;
    src[1] = 0;
    src[2] = 0;
    // vector from det pixel (0,0) to (1,0)
    // pix_u = pix * e_u
    pix_u[0] = pix * e_u[0];
    pix_u[1] = pix * e_u[1];
    pix_u[2] = pix * e_u[2];
    // vector from det pixel (0,0) to (0,1)
    // pix_v = pix * e_v
    pix_v[0] = pix * e_v[0];
    pix_v[1] = pix * e_v[1];
    pix_v[2] = pix * e_v[2];
    // center of detector (at a distance D from source, displaced by u_0/2 and v_0/2)
    // det = src    - D * e_w    + 0.5 * u_0 * e_u    + 0.5 * v_0 * e_v
    det[0] = src[0] - D * e_w[0] + 0.5 * u_0 * e_u[0] + 0.5 * v_0 * e_v[0];
    det[1] = src[1] - D * e_w[1] + 0.5 * u_0 * e_u[1] + 0.5 * v_0 * e_v[1];
    det[2] = src[2] - D * e_w[2] + 0.5 * u_0 * e_u[2] + 0.5 * v_0 * e_v[2];

    // rotate x -> -y (seems to work only this way!)
    gamma = -M_PI_2;
    rotate_z(gamma,src[0],src[1],src[2],&src[0],&src[1],&src[2]);
    rotate_z(gamma,det[0],det[1],det[2],&det[0],&det[1],&det[2]);
    rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&pix_u[0],&pix_u[1],&pix_u[2]);
    rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&pix_v[0],&pix_v[1],&pix_v[2]);

    // rotate by projection angle around Z
    angles = create_angles(0,2*M_PI,nproj);
    vectors = new double[12*nproj];
    for(i=0;i<nproj;i++) {
        gamma = angles[i];
        rotate_z(gamma,src[0],src[1],src[2],&vectors[12*i+0],&vectors[12*i+1],&vectors[12*i+2]);
        rotate_z(gamma,det[0],det[1],det[2],&vectors[12*i+3],&vectors[12*i+4],&vectors[12*i+5]);
        rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&vectors[12*i+6],&vectors[12*i+7],&vectors[12*i+8]);
        rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&vectors[12*i+9],&vectors[12*i+10],&vectors[12*i+11]);
    }
    delete[] angles;

    // create projection geometry
    proj_geom = create_proj_geom_3d_cone_vec(det_axial,det_perp,nproj,vectors);
    delete[] vectors;

    // create CUDA 3D projector
    proy = create_projector_3d_cuda(proj_geom,vol_geom);
    proy->setDetectorSuperSampling(3);

    // create data
    sinoData = create_data_3d_sino(proj_geom,sino_full_f); delete[] sino_full_f;
    volumeData = create_data_3d_vol(vol_geom,0.0f);

    /* =============
       EMULATE SIRT!
       ============= */

    // Memory usage is 3 x (volume data + projection data), once for input data,
    // another for output data, the third for precomputed weights

    const float32 relaxation = 1.99f; // epsilon = 0.005 (Gregor & Benson '08)
    const unsigned int maxiter = 200; // max iterations
    const double epsrel = 0.01; // convergence condition
    bool useMinConstraint = true;
    const float32 MinConstraint = 0.0;
    bool useMaxConstraint = true;
    const float32 MaxConstraint = 1.0;
    const int pauseiter = 50; // pause every this iterations
    const int pause = 300; // pause length in seconds
    double norm=HUGE_VAL,deltanorm;
    int converged = 0;
    unsigned int iter;

    // initial time  (seconds)
    gettimeofday(&tp,NULL); t0 = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"Sinogram: %s\n",file);
    
    // START init
    tmpData = create_data_3d_vol(vol_geom,1.0f);
    projData = create_data_3d_sino(proj_geom,1.0f);
    pixelWeight = create_data_3d_vol(vol_geom);
    lineWeight = create_data_3d_sino(proj_geom);
    delete vol_geom;
    delete proj_geom;
    // END init

    // START precompute weights
    cgeomgr->doFP(proy,tmpData,lineWeight);
    ptmp = lineWeight->getData();
#pragma omp parallel for private(ix)
    for(ix=0;ix<lineWeight->getSize();ix++)
        ptmp[ix] = ptmp[ix] > 0.000001f ? 1 / ptmp[ix] : 0.0f;
    save_data_3d("lineWeights.raw",lineWeight);
    exit(0);

    cgeomgr->doBP(proy,pixelWeight,projData);
    ptmp = pixelWeight->getData();
#pragma omp parallel for private(ix)
    for(ix=0;ix<pixelWeight->getSize();ix++)
        ptmp[ix] = (ptmp[ix] > 0.000001f ? 1 / ptmp[ix] : 0.0f) * relaxation;
    // END precompute weights
    
    // time at start of iterations (seconds)
    gettimeofday(&tp,NULL); t1 = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"Iteration started\n");

    // START iterate
    for(iter = 0; iter < maxiter; iter++) {
        // forward-project
        cgeomgr->doFP(proy,volumeData,projData);

        // compute norm
        ctmp = projData->getDataConst();
        ctmp2= sinoData->getDataConst();
        deltanorm = norm;
        norm = 0.0;
#pragma omp parallel for private(ix) reduction(+:norm)
        for(ix=0;ix<projData->getSize();ix++)
            norm += (ctmp2[ix] - ctmp[ix]) * (ctmp2[ix] - ctmp[ix]);
        norm = sqrt(norm);
        deltanorm -= norm;
        fprintf(stderr,"Norm of difference: %g (%g)\n",norm,deltanorm);

        // convergence condition
        if(fabs(deltanorm) < epsrel) {
            fprintf(stderr,"Iteration converged, finishing...\n");
            converged = 1;
            iter++;
            break;
        }

        // subtract projection from sinogram and apply line weights
        ptmp = projData->getData();
        ctmp = sinoData->getDataConst();
        ctmp2= lineWeight->getDataConst();
#pragma omp parallel for private(ix)
        for(ix=0;ix<projData->getSize();ix++)
            ptmp[ix] = (ctmp[ix] - ptmp[ix]) * ctmp2[ix];

        // back-project
        cgeomgr->doBP(proy,tmpData,projData);

        // apply pixel weights and sum with volume data
        ptmp = volumeData->getData();
        ctmp = tmpData->getDataConst();
        ctmp2= pixelWeight->getDataConst();
#pragma omp parallel for private(ix)
        for(ix=0;ix<volumeData->getSize();ix++)
            ptmp[ix] += ctmp[ix] * ctmp2[ix];
        
        // clamp min
        if(useMinConstraint) {
            ptmp = volumeData->getData();
#pragma omp parallel for private(ix)
            for(ix=0;ix<volumeData->getSize();ix++)
                if(ptmp[ix] < MinConstraint)
                    ptmp[ix] = MinConstraint;
        }

        // clamp max
        if(useMaxConstraint) {
            ptmp = volumeData->getData();
#pragma omp parallel for private(ix)
            for(ix=0;ix<volumeData->getSize();ix++)
                if(ptmp[ix] > MaxConstraint)
                    ptmp[ix] = MaxConstraint;
        }

        // idle time to cool GPU
        if(iter + 1 < maxiter && (iter + 1) % pauseiter == 0) {
            fprintf(stderr,"%d of %d (max) iterations: idling %ds to cool GPU... ",iter+1,maxiter,pause);
            sleep(pause);
            fprintf(stderr,"resuming execution\n");
        }
    }
    delete lineWeight;
    delete pixelWeight;
    delete tmpData;

    // final time (seconds)
    gettimeofday(&tp,NULL); t2 = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"Iteration ended\n");

    // compute final norm
    if(!converged) {
        cgeomgr->doFP(proy,volumeData,projData);
        ctmp = projData->getDataConst();
        ctmp2= sinoData->getDataConst();
        deltanorm = norm;
        norm = 0.0;
#pragma omp parallel for private(ix) reduction(+:norm)
        for(ix=0;ix<projData->getSize();ix++)
            norm += (ctmp2[ix] - ctmp[ix]) * (ctmp2[ix] - ctmp[ix]);
        norm = sqrt(norm);
        deltanorm -= norm;
    }
    delete sinoData;
    delete projData;
    delete cgeomgr;
    delete proy;

    // save data
    snprintf(fname,sizeof(fname),"%s-reco",file);
    save_data_3d(fname,volumeData);
    delete volumeData;
    
    // statistics
    printf("Sinogram: %s\n",file);
    printf("Iterations: %d\n",iter);
    printf("Final norm of difference: %g\n",norm);
    printf("Final norm delta: %g\n",deltanorm);
    dt = t2 - t0;
    if(dt < 60.0)
        printf("Total reconstruction time: %gs\n",dt);
    else if(dt < 3600.0)
        printf("Total reconstruction time: %gm\n",dt/60.0);
    else
        printf("Total reconstruction time: %gh\n",dt/3600.0);
    dt = (t2 - t1) / iter;
    if(dt < 60.0)
        printf("Mean time per iteration: %gs\n",dt);
    else
        printf("Mean time per iteration: %gm\n",dt/60.0);

    return 0;
}
