#include "astra/CompositeGeometryManager.h"
#include "cpp/creators.hpp"

#include <sys/time.h>
#include <zlib.h>
#include <omp.h>

enum operation {
    OP_PRODUCT,
    OP_SUM,
    OP_SINO_NORM,
    OP_NORM,
    OP_MASK_NORM
};

using namespace std;
using namespace astra;

inline static void rotate_z(double angle, double x, double y, double z, double *x_o, double *y_o, double *z_o)
{
    *x_o = cos(angle) * x - sin(angle) * y;
    *y_o = sin(angle) * x + cos(angle) * y;
    *z_o = z;
}

inline static size_t dataSize(CFloat32Data3DMemory *data) {
    return (size_t)data->getWidth() * data->getHeight() * data->getDepth();
}

/* NOTICE: adjust size of buffers according to disk transfer and processor speed */
#define BUFSIZE 0x1000000L  /* 64 MiB worth of floats */
static void bufferedOpGZ(const char *filename, CFloat32Data3DMemory *data, enum operation op, double *norm_o) {
    float32 *buf,*dat,val,*p;
    struct timeval tp;
    char opname[512];
    size_t ix,n,read;
    double ti,tf;
    double norm;
    int i,nbuf;
    gzFile zfp;

    gettimeofday(&tp,NULL); ti = tp.tv_sec + 1E-6 * tp.tv_usec;
    zfp = gzopen(filename,"rb");
    gzbuffer(zfp,BUFSIZE);
    ASTRA_ASSERT(zfp != NULL);
    dat = data->getData();
    buf = new float32[BUFSIZE];
    nbuf = dataSize(data) / BUFSIZE;
    if(dataSize(data) % BUFSIZE > 0) nbuf++;
    if(op == OP_SINO_NORM || op == OP_NORM || op == OP_MASK_NORM) *norm_o = 0.0;
    read = 0;
    for(i=0;i<nbuf;i++) {
        n = gzfread(buf,sizeof(float32),BUFSIZE,zfp);
        ASTRA_ASSERT(n == BUFSIZE || gzeof(zfp));
        switch(op) {
            case OP_PRODUCT:
                if(read == 0)
                    snprintf(opname,sizeof(opname),"PRODUCT");
#pragma omp parallel for private(ix)
                for(ix=0;ix<n;ix++)
                    dat[ix + read] *= buf[ix];
                break;
            case OP_SUM:
                if(read == 0)
                    snprintf(opname,sizeof(opname),"SUM");
#pragma omp parallel for private(ix)
                for(ix=0;ix<n;ix++)
                    dat[ix + read] += buf[ix];
                break;
            case OP_SINO_NORM:
                if(read == 0)
                    snprintf(opname,sizeof(opname),"SINO_NORM");
                norm = 0.0;
#pragma omp parallel for private(ix,p) reduction(+:norm)
                for(ix=0;ix<n;ix++) {
                    p = &dat[ix + read];
                    *p = buf[ix] - *p;
                    norm += *p * *p;
                }
                *norm_o += norm;
                break;
            case OP_NORM:
                if(read == 0)
                    snprintf(opname,sizeof(opname),"NORM");
                norm = 0.0;
#pragma omp parallel for private(ix,val) reduction(+:norm)
                for(ix=0;ix<n;ix++) {
                    val = buf[ix] - dat[ix + read];
                    norm += val * val;
                }
                *norm_o += norm;
                break;
            case OP_MASK_NORM:
                if(read == 0)
                    snprintf(opname,sizeof(opname),"MASK_NORM");
                norm = 0.0;
#pragma omp parallel for private(ix,p) reduction(+:norm)
                for(ix=0;ix<n;ix++) {
                    p = &dat[ix + read];
                    if(buf[ix] == 0.0) *p = 0.0;
                    norm += *p * *p;
                }
                *norm_o += norm;
                break;
        }
        read += n;
        fprintf(stderr,"%s BUFFER %d/%d\n",opname,i+1,nbuf);
    }
    delete[] buf;
    gzclose(zfp);
    gettimeofday(&tp,NULL); tf = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"BUFFERED %s TOOK %g MINUTES\n",opname,(tf-ti)/60.0);
}

// nx * ny * nz are the volume dimensions and (x0,y0,z0) is the volume center
CVolumeGeometry3D* createVolGeom(double voxel_size, int nx, int ny, int nz, double x0, double y0, double z0) {
    const double xmin = x0 - (voxel_size * nx) * 0.5;
    const double xmax = x0 + (voxel_size * nx) * 0.5;
    const double ymin = y0 - (voxel_size * ny) * 0.5;
    const double ymax = y0 + (voxel_size * ny) * 0.5;
    const double zmin = z0 - (voxel_size * nz) * 0.5;
    const double zmax = z0 + (voxel_size * nz) * 0.5;

    return create_vol_geom_3d(nx,ny,nz,xmin,xmax,ymin,ymax,zmin,zmax);
}

// dim * dim * dim are the volume dimensions and (x0,y0,z0) is the volume center
CVolumeGeometry3D* createVolGeom(double voxel_size, int dim, double x0, double y0, double z0) {
    const double xmin = x0 - (voxel_size * dim) * 0.5;
    const double xmax = x0 + (voxel_size * dim) * 0.5;
    const double ymin = y0 - (voxel_size * dim) * 0.5;
    const double ymax = y0 + (voxel_size * dim) * 0.5;
    const double zmin = z0 - (voxel_size * dim) * 0.5;
    const double zmax = z0 + (voxel_size * dim) * 0.5;

    return create_vol_geom_3d(dim,dim,dim,xmin,xmax,ymin,ymax,zmin,zmax);
}

typedef struct {
    double R;
    double D;
    double u_0;
    double v_0;
    double twist;
    double slant;
    double tilt;
} coneBeamParams;

CConeVecProjectionGeometry3D* createConeProjGeom(double pix, int det_perp, int nproj, int det_axial, coneBeamParams *cbp) {
    CConeVecProjectionGeometry3D *proj_geom;
    const double eta = cbp->twist * M_PI / 180.0; // in-plane angle
    const double theta = cbp->slant * M_PI / 180.0; // polar angle
    const double phi = cbp->tilt  * M_PI / 180.0; // azimuthal angle
    double e_w[3],e_u[3],e_v[3],alpha[3],beta[3];
    double src[3],det[3],pix_u[3],pix_v[3];
    float32 *angles;
    double *vectors;
    double gamma;
    int i;

    /* ===================================
       CONE BEAM GEOMETRY (Noo et al. '00)
       =================================== */

    // normal to detector plane
    e_w[0] = cos(theta) * cos(phi);
    e_w[1] = cos(theta) * sin(phi);
    e_w[2] = sin(theta);

    // x orthogonal direction from e_w
    alpha[0] = -sin(phi);
    alpha[1] =  cos(phi);
    alpha[2] =  0;

    // y orthogonal direction from e_w
    beta[0] = -sin(theta) * cos(phi);
    beta[1] = -sin(theta) * sin(phi);
    beta[2] =  cos(theta);

    // x direction along detector plane (including in-plane rotation)
    e_u[0] = cos(eta) * alpha[0] + sin(eta) * beta[0];
    e_u[1] = cos(eta) * alpha[1] + sin(eta) * beta[1];
    e_u[2] = cos(eta) * alpha[2] + sin(eta) * beta[2];

    // y direction along detector plane (including in-plane rotation)
    e_v[0] = cos(eta) * beta[0] - sin(eta) * alpha[0];
    e_v[1] = cos(eta) * beta[1] - sin(eta) * alpha[1];
    e_v[2] = cos(eta) * beta[2] - sin(eta) * alpha[2];

    // geometry at projection angle 0
    // cone vertex (source)
    src[0] = cbp->R;
    src[1] = 0;
    src[2] = 0;
    // vector from det pixel (0,0) to (1,0)
    // pix_u = pix * e_u
    pix_u[0] = pix * e_u[0];
    pix_u[1] = pix * e_u[1];
    pix_u[2] = pix * e_u[2];
    // vector from det pixel (0,0) to (0,1)
    // pix_v = pix * e_v
    pix_v[0] = pix * e_v[0];
    pix_v[1] = pix * e_v[1];
    pix_v[2] = pix * e_v[2];
    // center of detector (at a distance D from source, displaced by u_0/2 and v_0/2)
    // det = src    - D * e_w    + 0.5 * u_0 * e_u    + 0.5 * v_0 * e_v
    det[0] = src[0] - cbp->D * e_w[0] + 0.5 * cbp->u_0 * e_u[0] + 0.5 * cbp->v_0 * e_v[0];
    det[1] = src[1] - cbp->D * e_w[1] + 0.5 * cbp->u_0 * e_u[1] + 0.5 * cbp->v_0 * e_v[1];
    det[2] = src[2] - cbp->D * e_w[2] + 0.5 * cbp->u_0 * e_u[2] + 0.5 * cbp->v_0 * e_v[2];

    // rotate x -> -y (seems to work only this way!)
    gamma = -M_PI_2;
    rotate_z(gamma,src[0],src[1],src[2],&src[0],&src[1],&src[2]);
    rotate_z(gamma,det[0],det[1],det[2],&det[0],&det[1],&det[2]);
    rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&pix_u[0],&pix_u[1],&pix_u[2]);
    rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&pix_v[0],&pix_v[1],&pix_v[2]);

    // rotate by projection angle around Z
    angles = create_angles(0,2*M_PI,nproj);
    vectors = new double[12*nproj];
    for(i=0;i<nproj;i++) {
        gamma = angles[i];
        rotate_z(gamma,src[0],src[1],src[2],&vectors[12*i+0],&vectors[12*i+1],&vectors[12*i+2]);
        rotate_z(gamma,det[0],det[1],det[2],&vectors[12*i+3],&vectors[12*i+4],&vectors[12*i+5]);
        rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&vectors[12*i+6],&vectors[12*i+7],&vectors[12*i+8]);
        rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&vectors[12*i+9],&vectors[12*i+10],&vectors[12*i+11]);
    }
    delete[] angles;

    proj_geom = create_proj_geom_3d_cone_vec(det_axial,det_perp,nproj,vectors);
    delete[] vectors;

    return proj_geom;
}

int main (int argc, char *argv[])
{
    CCompositeGeometryManager *cgeomgr;
    CFloat32ProjectionData3DMemory *projData;
    CConeVecProjectionGeometry3D *proj_geom;
    CFloat32VolumeData3DMemory *tmpData;
    CVolumeGeometry3D *vol_geom;
    CCudaProjector3D *proy;

    char fname[1024];
    float32 *ptmp;
    char *file;

    double t0,t1,t2,dt;
    struct timeval tp;

    // cone beam parameters
    // TODO: doesn't work, HR is blurry
    coneBeamParams cbp = {
        // CTGeometryCalibration.SQL
        //298.826992549658,   // R (mm)
        //451.893489033932,   // D (mm)
        //-4.82925785256236,  // u_0 (mm)
        //-3.59317804001221,  // v_0 (mm)
        //0.0989913819760175, // twist (deg)
        //0.155140425306331,  // slant (deg)
        //-0.277073673982133  // tilt (deg)

        // current in DB and RAW headers
        295.1956,   // R (mm)
        446.9855,   // D (mm)
        -3.639418,  // u_0 (mm)
        -1.355065,  // v_0 (mm)
        -0.1292242, // twist (deg)
        -0.3939571, // slant (deg)
        -0.1672924  // tilt (deg)

        // albira.geom
        //298.986,    // R (mm)
        //452.185,    // D (mm)
        //-2.50153,   // u_0 (mm)
        //0.578633,   // v_0 (mm)
        //0.0993233,  // twist (deg)
        //-0.141372,  // slant (deg)
        //0.261506    // tilt (deg)

        // CTCalibrations.txt
        //298.83809,      // R (mm)
        //451.91068,      // D (mm)
        //-4.8292772,     // u_0 (mm)
        //-3.5932295,     // v_0 (mm)
        //0.098996993,    // twist (deg)
        //0.15513352,     // slant (deg)
        //-0.27706844     // tilt (deg)
    };

    ASTRA_ASSERT(argc >= 2);
    file = realpath(argv[1],NULL);
    ASTRA_ASSERT(file != NULL);

    /* =============
       EMULATE SIRT!
       ============= */

    // Memory usage is (reconstruction volume + sinogram) plus temporary buffer space

    const float32 relaxation = 1.99f; // epsilon = 0.005 (Gregor & Benson '08)
    const unsigned int maxiter = 100; // max iterations
    const double epsabs = 0.01; // convergence tolerance
    const bool useMinConstraint = true;
    const float32 MinConstraint = 0.0;
    const bool useMaxConstraint = true;
    const float32 MaxConstraint = 1.0;

    bool resume = true;
    bool converged = false;
    double norm,deltanorm;
    unsigned int iter;
    size_t ix;
    FILE *fp;

    // initial time  (seconds)
    gettimeofday(&tp,NULL); t0 = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"Sinogram: %s\n",file);

    // START alloc
    // create composite geometry manager
    cgeomgr = new CCompositeGeometryManager();
    // create volume geometry
    vol_geom = createVolGeom(0.050,1560,
            0.0,0.0,-2.0); // FOV center in mm
    // create projection geometry
    proj_geom = createConeProjGeom(0.05,2240,1000,2240,&cbp);
    // create CUDA 3D projector
    proy = create_projector_3d_cuda(proj_geom,vol_geom);
    //proy->setDetectorSuperSampling(3);
    tmpData = create_data_3d_vol(vol_geom); delete vol_geom;
    projData = create_data_3d_sino(proj_geom); delete proj_geom;
    fprintf(stderr,"ALLOC DONE\n");
    // END alloc

    // START precompute weights
    fp = fopen("lineWeight.gz","r");
    if(fp != NULL) fclose(fp);
    else { /* non-existing */
        tmpData->setData(1.0f);
        cgeomgr->doFP(proy,tmpData,projData);
        fprintf(stderr,"PROJECTION DONE\n");
        ptmp = projData->getData();
#pragma omp parallel for private(ix)
        for(ix=0;ix<dataSize(projData);ix++)
            ptmp[ix] = ptmp[ix] > 0.000001f ? 1 / ptmp[ix] : 0.0f;
        save_data_3d_gz("lineWeight.gz",projData);
        fprintf(stderr,"LINE WEIGHTS COMPUTED\n");
    }

    fp = fopen("pixelWeight.gz","r");
    if(fp != NULL) fclose(fp);
    else { /* non-existing */
        projData->setData(1.0f);
        cgeomgr->doBP(proy,tmpData,projData);
        fprintf(stderr,"BACKPROJECTION DONE\n");
        ptmp = tmpData->getData();
#pragma omp parallel for private(ix)
        for(ix=0;ix<dataSize(tmpData);ix++)
            ptmp[ix] = (ptmp[ix] > 0.000001f ? 1 / ptmp[ix] : 0.0f) * relaxation;
        save_data_3d_gz("pixelWeight.gz",tmpData);
        fprintf(stderr,"PIXEL WEIGHTS COMPUTED\n");
    }
    // END precompute weights
    
    // START init
    if(resume) {
        norm = 0.0;
        tmpData = load_data_3d_vol_gz("volumeData.gz",tmpData);
    } else
        projData = load_data_3d_sino_gz(file,projData);
    fprintf(stderr,"INIT DONE\n");
    // END init

    // time at start of iterations (seconds)
    gettimeofday(&tp,NULL); t1 = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"Iteration start\n");

    // START iterate
    for(iter = resume; iter < maxiter; iter++) {
        fprintf(stderr,"ITERATION %d\n",iter);

        if(iter > 0) {
            // forward-project
            cgeomgr->doFP(proy,tmpData,projData);
            fprintf(stderr,"PROJECTION DONE\n");

            // compute norm and update sinogram
            deltanorm = norm;
            bufferedOpGZ("sinoData.gz",projData,OP_SINO_NORM,&norm);
            norm = sqrt(norm);
            deltanorm = norm - deltanorm;
            fprintf(stderr,"Updated norm of difference: %g (%+g)\n",norm,deltanorm);

            // convergence condition
            if(fabs(deltanorm) < epsabs) {
                fprintf(stderr,"Iteration converged, finishing...\n");
                converged = true;
                iter++;
                break; 
            }
        } else {
            // mask sinogram and compute norm
            bufferedOpGZ("lineWeight.gz",projData,OP_MASK_NORM,&norm);
            norm = sqrt(norm);
            fprintf(stderr,"Initial norm of difference: %g\n",norm);
            // save masked sinogram
            save_data_3d_gz("sinoData.gz",projData);
            fprintf(stderr,"SINO MASK DONE\n");
        }

        // apply line weights
        bufferedOpGZ("lineWeight.gz",projData,OP_PRODUCT,NULL);
        fprintf(stderr,"LINE WEIGHTS APPLIED\n");

        // back-project
        cgeomgr->doBP(proy,tmpData,projData);
        fprintf(stderr,"BACKPROJECTION DONE\n");

        // apply pixel weights
        bufferedOpGZ("pixelWeight.gz",tmpData,OP_PRODUCT,NULL);
        fprintf(stderr,"PIXEL WEIGHTS APPLIED\n");

        // update reconstruction
        if(iter > 0) {
            bufferedOpGZ("volumeData.gz",tmpData,OP_SUM,NULL);
            fprintf(stderr,"UPDATE RECONSTRUCTION DONE\n");
        }
        
        // clamp min
        if(useMinConstraint) {
            ptmp = tmpData->getData();
#pragma omp parallel for private(ix)
            for(ix=0;ix<dataSize(tmpData);ix++)
                if(ptmp[ix] < MinConstraint)
                    ptmp[ix] = MinConstraint;
            fprintf(stderr,"CLAMP MIN DONE\n");
        }

        // clamp max
        if(useMaxConstraint) {
            ptmp = tmpData->getData();
#pragma omp parallel for private(ix)
            for(ix=0;ix<dataSize(tmpData);ix++)
                if(ptmp[ix] > MaxConstraint)
                    ptmp[ix] = MaxConstraint;
            fprintf(stderr,"CLAMP MAX DONE\n");
        }

        // save reconstruction
        save_data_3d_gz("volumeData.gz",tmpData);
        fprintf(stderr,"SAVE DONE\n");
    }

    // final time (seconds)
    gettimeofday(&tp,NULL); t2 = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"Iteration end\n");

    // compute final norm
    if(!converged) {
        cgeomgr->doFP(proy,tmpData,projData);
        fprintf(stderr,"PROJECTION DONE\n");
        deltanorm = norm;
        bufferedOpGZ("sinoData.gz",projData,OP_NORM,&norm);
        norm = sqrt(norm);
        deltanorm = norm - deltanorm;
    }

    // SART dealloc
    delete tmpData;
    delete projData;
    delete cgeomgr;
    delete proy;
    fprintf(stderr,"DEALLOC DONE\n");
    // END dealloc
    
    // START statistics
    printf("Sinogram: %s\n",file);
    printf("Iterations: %d\n",iter);
    printf("Final norm of difference: %g\n",norm);
    printf("Final norm delta: %+g\n",deltanorm);
    dt = t2 - t0;
    if(dt < 60.0)
        printf("Total reconstruction time: %gs\n",dt);
    else if(dt < 3600.0)
        printf("Total reconstruction time: %gm\n",dt/60.0);
    else
        printf("Total reconstruction time: %gh\n",dt/3600.0);
    dt = (t2 - t1) / iter;
    if(dt < 60.0)
        printf("Mean time per iteration: %gs\n",dt);
    else
        printf("Mean time per iteration: %gm\n",dt/60.0);
    // END statistics

    // START cleanup
    unlink("lineWeight.gz");
    unlink("pixelWeight.gz");
    unlink("sinoData.gz");
    snprintf(fname,sizeof(fname),"%s-reco.gz",file);
    rename("volumeData.gz",fname);
    fprintf(stderr,"CLEANUP DONE");
    // END cleanup

    return 0;
}
